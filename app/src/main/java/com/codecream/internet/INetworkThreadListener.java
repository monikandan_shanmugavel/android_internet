package com.codecream.internet;

public interface INetworkThreadListener {
    public void onRequestComplete(String url, String resultStr);
}
