package com.codecream.internet;

import android.os.AsyncTask;
import android.util.Log;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

public class NetworkThread extends AsyncTask<Void, Void, Boolean> {

    private String url;
    private INetworkThreadListener listener;
    private String apiResult;

    public NetworkThread(String url, INetworkThreadListener listener){
        this.url = url;
        this.listener = listener;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(this.url).build();
        try {
            Response response = client.newCall(request).execute();
            String result = response.body().string();
            Log.i("internet", result);
            apiResult = result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Boolean result){
        listener.onRequestComplete(this.url, this.apiResult);
    }
}
